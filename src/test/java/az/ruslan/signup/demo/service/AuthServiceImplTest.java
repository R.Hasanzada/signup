package az.ruslan.signup.demo.service;

import az.ruslan.signup.demo.domain.MsUser;
import az.ruslan.signup.demo.dto.SignupDto;
import az.ruslan.signup.demo.exception.EmailIsAlreadyUseException;
import az.ruslan.signup.demo.exception.PasswordDoNotMatchException;
import az.ruslan.signup.demo.repo.MsUserRepository;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.stubbing.Answer;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;


@ExtendWith(MockitoExtension.class)
class AuthServiceImplTest {


    @Mock
    private MsUserRepository msUserRepository;

    private AuthServiceImpl authService;

    @BeforeEach
    void init(){
        authService = new AuthServiceImpl(msUserRepository);
    }

    @Test
    void givenEmailIsAlreadyUsed(){
        //Arrange
        SignupDto signupDto = new SignupDto();

        signupDto.setEmail("ruslan@gmail.com");
        signupDto.setPassword("r12345678");
        signupDto.setPasswordConf("r12345678");

        MsUser msUser = new MsUser();
        msUser.setEmail(signupDto.getEmail());
        msUser.setPassword(signupDto.getPassword());
        msUser.setPasswordConf(signupDto.getPasswordConf());

        //act
        Mockito.when(msUserRepository.findByEmail(signupDto.getEmail())).thenReturn(msUser);

        Assertions.assertThatThrownBy(() -> authService.signUp(signupDto))
                .isInstanceOf(EmailIsAlreadyUseException.class);
    }

    @Test
    void givenPasswordDoNotMatchThenException(){
        //Arrange
        SignupDto signupDto = new SignupDto();
        signupDto.setPassword("1234");
        signupDto.setPasswordConf("456");

        //Act & Assert
        Assertions.assertThatThrownBy(() -> authService.signUp(signupDto))
                .isInstanceOf(PasswordDoNotMatchException.class);
    }


}