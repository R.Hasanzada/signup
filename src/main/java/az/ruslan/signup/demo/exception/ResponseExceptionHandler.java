package az.ruslan.signup.demo.exception;

import az.ruslan.signup.demo.dto.ErrorMessage;
import lombok.extern.slf4j.Slf4j;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.Date;


@Slf4j
@ControllerAdvice
public class ResponseExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(value = {PasswordDoNotMatchException.class})
    public ResponseEntity<Object> handleSpecificException(RuntimeException ex, WebRequest req) {

        String errMessage = ex.getLocalizedMessage();
        if (errMessage == null) errMessage = ex.toString();

        ErrorMessage errorMessage = new ErrorMessage(new Date(), errMessage);
        return new ResponseEntity<>(
                errorMessage, new HttpHeaders(), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(value = {ValidationException.class})
    public ResponseEntity<Object> handleValidationException(ValidationException ex, WebRequest req) {

        String errMessage = ex.getMessage();
        if (errMessage == null) errMessage = ex.toString();

        ErrorMessage errorMessage = new ErrorMessage(new Date(), errMessage);
        return new ResponseEntity<>(
                errorMessage, new HttpHeaders(), HttpStatus.BAD_REQUEST);
    }

}
