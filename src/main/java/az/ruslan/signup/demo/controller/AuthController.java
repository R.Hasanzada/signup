package az.ruslan.signup.demo.controller;

import az.ruslan.signup.demo.dto.SignupDto;
import az.ruslan.signup.demo.exception.ValidationException;
import az.ruslan.signup.demo.service.AuthService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/v1/auth")
@RequiredArgsConstructor
public class AuthController {


    private final AuthService authService;

    @PostMapping("/sign-up")
    public ResponseEntity<SignupDto> signup(@RequestBody @Valid SignupDto signupDto, BindingResult errors){

        if (errors.hasErrors()) {
            throw new ValidationException( errors);
        }

        authService.signUp(signupDto);

        return new ResponseEntity<>(HttpStatus.OK);
    }

}
