package az.ruslan.signup.demo.repo;

import az.ruslan.signup.demo.domain.MsUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MsUserRepository extends JpaRepository<MsUser, Long> {

    MsUser findByEmail(String email);
}
