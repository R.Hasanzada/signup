package az.ruslan.signup.demo.service;

import az.ruslan.signup.demo.dto.SignupDto;
import az.ruslan.signup.demo.exception.EmailIsAlreadyUseException;

public interface AuthService {

    void signUp(SignupDto signupDto) throws EmailIsAlreadyUseException;

}
