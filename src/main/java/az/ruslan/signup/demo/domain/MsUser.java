package az.ruslan.signup.demo.domain;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
@Table(name = "ms_user")
public class MsUser {

    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private String email;

    @Column(nullable = false)
    private String password;

    @Column(nullable = false)
    private String passwordConf;

    @Column
    private String privacyPolicy;


}
