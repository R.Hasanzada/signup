FROM alpine:3.11.2
RUN apk add --no-cache openjdk8
COPY /build/libs/demo-0.0.1-SNAPSHOT.jar /app/
WORKDIR /app/
ENTRYPOINT ["java"]
CMD ["-jar", "/app/demo-0.0.1-SNAPSHOT.jar"]
