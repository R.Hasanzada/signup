package az.ruslan.signup.demo.service;

import az.ruslan.signup.demo.domain.MsUser;
import az.ruslan.signup.demo.dto.SignupDto;
import az.ruslan.signup.demo.exception.EmailIsAlreadyUseException;
import az.ruslan.signup.demo.exception.PasswordDoNotMatchException;
import az.ruslan.signup.demo.repo.MsUserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@RequiredArgsConstructor
@Service
public class AuthServiceImpl implements AuthService{

    private final MsUserRepository msUserRepository;

    
    @Override
    public void signUp(SignupDto signupDto) throws RuntimeException {
        if(!signupDto.getPassword().equals(signupDto.getPasswordConf()))
            throw new PasswordDoNotMatchException();

        if(msUserRepository.findByEmail(signupDto.getEmail()) != null)
            throw new EmailIsAlreadyUseException();

        MsUser msUser = new MsUser();
        msUser.setEmail(signupDto.getEmail());
        msUser.setPassword(signupDto.getPassword());
        msUser.setPasswordConf(signupDto.getPasswordConf());
        msUser.setPrivacyPolicy(signupDto.getPrivacyPolicy());

        msUserRepository.save(msUser);
    }


}
