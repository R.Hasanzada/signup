package az.ruslan.signup.demo.exception;

public class EmailIsAlreadyUseException extends RuntimeException{

    private static final String MESSAGE = "Email is already use";

    public EmailIsAlreadyUseException(){
        super(MESSAGE);
    }
}
