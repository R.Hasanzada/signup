package az.ruslan.signup.demo.dto;

import lombok.Data;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
public class SignupDto {

    @Email
    private String email;

    @NotNull(message = "password can not be null")
    @Size(min = 8, max = 30, message = "password must be equal or greater than 8 character and less than 30 character")
    private String password;

    @NotNull(message = "password can not be null")
    @Size(min = 8, max = 30, message = "password must be equal or greater than 8 character and less than 30 character")
    private String passwordConf;

    private String privacyPolicy;

}
