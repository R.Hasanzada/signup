package az.ruslan.signup.demo.controller;

import az.ruslan.signup.demo.dto.SignupDto;
import az.ruslan.signup.demo.service.AuthServiceImpl;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;


@AutoConfigureMockMvc
@ExtendWith(SpringExtension.class)
@WebMvcTest(AuthController.class)
class AuthControllerTest {

    @MockBean
    private AuthServiceImpl authService;

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @Test
    void whenInvalidDtoBadRequestForNullData() throws Exception {

        SignupDto signupDto = new SignupDto();

        mockMvc.perform(
                post("/v1/auth/sign-up")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(signupDto))
                ).andExpect(status().isBadRequest());
    }

    @Test
    void whenInvalidEmailBadRequestFor() throws Exception {

        SignupDto signupDto = new SignupDto();
        signupDto.setPassword("12345677");
        signupDto.setPasswordConf("12345677");
        signupDto.setEmail("mail.com");

        mockMvc.perform(
                post("/v1/auth/sign-up")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(signupDto))
        ).andExpect(status().isBadRequest());
    }

    @Test
    void whenInvalidPasswordSizeMinBadRequestFor() throws Exception {

        SignupDto signupDto = new SignupDto();
        signupDto.setPassword("123");
        signupDto.setPasswordConf("123");
        signupDto.setEmail("email");

        mockMvc.perform(
                post("/v1/auth/sign-up")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(signupDto))
        ).andExpect(status().isBadRequest());
    }

    @Test
    void whenInvalidPasswordSizeMaxBadRequestFor() throws Exception {

        SignupDto signupDto = new SignupDto();
        String longPassword = "visa WALMART 9 \" MUSIC FRUIT \\ % APPLE 8 egg drip SKYPE @ , ^ 6 fruit ] NUT drip 8 tokyo jack tokyo 5 NUT USA 5 + 7 2";
        signupDto.setPassword(longPassword);
        signupDto.setPasswordConf(longPassword);
        signupDto.setEmail("email@mail.ru");

        mockMvc.perform(
                post("/v1/auth/sign-up")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(signupDto))
        ).andExpect(status().isBadRequest());
    }

}